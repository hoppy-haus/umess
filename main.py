import sys, time
import socket
import fcntl
import struct
from threading import Thread
import hashlib
import errno

buffer = ''
clients = set()
breaker = 1

def get_ip_address(ifname):
    ss = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        ss.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

MYPORT = 50000
servmsg = "Umess V1: " + get_ip_address('wlan0')
print servmsg
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(('', 0))
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
class broadcast(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()
    def run(self):
        while 1:
            data = servmsg
            s.sendto(data, ('<broadcast>', MYPORT))
            time.sleep(2)

def handler(hcon, hadd):
    while 1:
        line = buffer+hcon.recv(1024).strip()
        if len(line) > 0:
            for i in clients:
                i.send(hashlib.md5(hadd[0]).digest().encode("base64") + ' | ' + line)
                print hadd[0] + " | " + line
        if not line:
            break

def sendall(mess, hadd):
    for i in clients:
        i.send(hashlib.md5(hadd[0]).digest().encode("base64") + " disconnected")

def disconn(hcon, hadd):
    while 1:
        for i in clients.copy():
            try:
                i.send('ping')
            except socket.error, e:
                if e.errno == errno.EPIPE:
                    i.close()
                    clients.remove(i)
                    print hadd[0] + " disconnected."
                else:
                    print e
                    pass
        time.sleep(1)

class serv(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()
    def run(self):
        sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sc.bind(('', 6783))
        except socket.error as msg:
            sys.exit()
        sc.listen(10)
        sc.settimeout(float("inf"))
        while 1:
            conn, addr = sc.accept()
            hnd = Thread(target=handler, args=(conn, addr))
            hnd.start()
            dsc = Thread(target=disconn, args=(conn, addr))
            dsc.start()
            breaker = 0
            print "breaked"
            clients.add(conn)
            
broadcast()
serv()
while 1:
    pass